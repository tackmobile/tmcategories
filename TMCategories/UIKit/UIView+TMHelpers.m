
#import "UIView+TMHelpers.h"

@implementation UIView (TMHelpers)


#pragma mark - BOUNDS POSITION GETTERS -

- (CGFloat)boundsBottom {
  return self.bounds.origin.y + self.bounds.size.height;
}

- (CGFloat)boundsLeft {
  return self.bounds.origin.x;
}

- (CGFloat)boundsRight {
  return self.bounds.origin.x + self.bounds.size.width;
}

- (CGFloat)boundsTop {
  return self.bounds.origin.y;
}

- (CGPoint)boundsPosition {
  return self.bounds.origin;
}

- (CGPoint)boundsCenter {
  CGRect bounds = self.bounds;
  
  return CGPointMake(CGRectGetMidX(bounds), CGRectGetMidY(bounds));
}


#pragma mark - BOUNDS SIZE GETTERS -

- (CGFloat)boundsHeight {
  return self.bounds.size.height;
}

- (CGFloat)boundsWidth {
  return self.bounds.size.width;
}

- (CGSize)boundsSize {
  return self.bounds.size;
}


#pragma mark - POSITION GETTERS -

- (CGFloat)frameBottom {
  return self.frame.origin.y + self.frame.size.height;
}

- (CGFloat)frameLeft {
  return self.frame.origin.x;
}

- (CGFloat)frameRight {
  return self.frame.origin.x + self.frame.size.width;
}

- (CGFloat)frameTop {
  return self.frame.origin.y;
}

- (CGPoint)framePosition {
  return self.frame.origin;
}


#pragma mark - POSITION SETTERS -

- (void)setFrameBottom:(CGFloat)frameBottom {
  self.frame = CGRectMake(self.frameLeft, frameBottom - self.frameHeight, self.frameWidth, self.frameHeight);
}

- (void)setFrameLeft:(CGFloat)frameLeft {
  self.frame = CGRectMake(frameLeft, self.frameTop, self.frameWidth, self.frameHeight);
}

- (void)setFrameRight:(CGFloat)frameRight {
  self.frame = CGRectMake(frameRight - self.frameWidth, self.frameTop, self.frameWidth, self.frameHeight);
}

- (void)setFrameTop:(CGFloat)frameTop {
  self.frame = CGRectMake(self.frameLeft, frameTop, self.frameWidth, self.frameHeight);
}

- (void)setFramePosition:(CGPoint)framePosition {
  self.frame = CGRectMake(framePosition.x, framePosition.y, self.frameWidth, self.frameHeight);
}

- (CGFloat)frameX {
  return self.frame.origin.x;
}

- (void)setFrameX:(CGFloat)newX {
  self.frame = CGRectMake(newX, self.frame.origin.y, self.frame.size.width, self.frame.size.height);
}

- (CGFloat)frameY {
  return self.frame.origin.y;
}

- (void)setFrameY:(CGFloat)newY {
  self.frame = CGRectMake(self.frame.origin.x, newY, self.frame.size.width, self.frame.size.height);
}


#pragma mark - SIZE GETTERS -

- (CGFloat)frameHeight {
  return self.frame.size.height;
}

- (CGFloat)frameWidth {
  return self.frame.size.width;
}

- (CGSize)frameSize {
  return CGSizeMake(self.frameWidth, self.frameHeight);
}


#pragma mark - SCREEN SIZE -

- (void)setFrameHeight:(CGFloat)frameHeight {
  self.frame = CGRectMake(self.frameLeft, self.frameTop, self.frameWidth, frameHeight);
}

- (void)setFrameWidth:(CGFloat)frameWidth {
  self.frame = CGRectMake(self.frameLeft, self.frameTop, frameWidth, self.frameHeight);
}

- (void)setFrameSize:(CGSize)frameSize {
  self.frame = CGRectMake(self.frameLeft, self.frameTop, frameSize.width, frameSize.height);
}

#if !defined(TM_APP_EXTENSIONS)
// NB: [UIApplication sharedApplication] is not supported for App Extensions.
//     If you are encountering a build error here for that reason, define
//     the TM_APP_EXTENSIONS macro so the offending logic in this method is
//     not compiled into your application.
//
//     c.f. https://github.com/AFNetworking/AFNetworking/issues/2119
+ (CGRect)getScreenBoundsForCurrentOrientation {
  UIInterfaceOrientation orientation = [[UIApplication sharedApplication] statusBarOrientation];
  return [UIView getScreenBoundsForOrientation:orientation];
}
#endif

+ (CGRect)getScreenBoundsForOrientation:(UIInterfaceOrientation)orientation {
  CGRect fullScreenRect = [UIScreen mainScreen].bounds;
  
  if (UIInterfaceOrientationIsLandscape(orientation)) {
    CGRect temp = CGRectMake(0, 0, fullScreenRect.size.height, fullScreenRect.size.width);
    fullScreenRect = temp;
  }
  
  return fullScreenRect;
}

@end