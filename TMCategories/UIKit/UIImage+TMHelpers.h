
@interface UIImage (TMHelpers)

- (UIColor *)averageColor;
- (UIImage *)tintedImageWithColor:(UIColor *)tintColor;

@end