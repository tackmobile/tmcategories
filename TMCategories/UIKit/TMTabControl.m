
#import "TMTabControl.h"
#import "UIView+TMHelpers.h"

@interface TMTabControl ()

@property (strong, nonatomic) NSArray   *tabs;
@property (strong, nonatomic) NSNumber  *itemSpacing;
@property (strong, nonatomic) UIColor   *normalBackgroundColor, *highlightedBackgroundColor;
@property (strong, nonatomic) UIView    *selectedTabView;

@property (assign, nonatomic) NSInteger selectedIndex;

@end

@implementation TMTabControl

- (void)awakeFromNib {
	self.backgroundColor = [UIColor clearColor];
}

- (void)layoutSubviews {
	__block CGFloat itemWidth = 0.0f;
	
	if (self.itemSpacing) {
		itemWidth = (self.frameWidth - ([self.itemSpacing floatValue] * (self.tabs.count - 1))) / self.tabs.count;
	} else {
		itemWidth = self.frameWidth / self.tabs.count;
	}
	
	[self.tabs enumerateObjectsUsingBlock:^(UILabel *tabLabel, NSUInteger idx, BOOL *stop) {
		CGFloat xPosition = (itemWidth * idx) + (idx > 0 ? [self.itemSpacing floatValue] * idx : 0);
		
		tabLabel.frameWidth = itemWidth;
		tabLabel.frameLeft = xPosition;
	}];
	
	if (self.selectedTabView) {
		self.selectedTabView.frameWidth = itemWidth;
		self.selectedTabView.frameLeft = (itemWidth * self.selectedIndex) + (self.selectedIndex > 0 ? [self.itemSpacing floatValue] * self.selectedIndex : 0);
	}
}


#pragma mark - INSTANCE METHODS -

- (void)configureWithItems:(NSArray *)items itemSpacing:(NSNumber *)itemSpacing tabLocation:(TMTabLocation)tabLocation textAttributes:(NSDictionary *)textAttributes {
	self.tintColor    = [super tintColor];
	self.itemSpacing  = itemSpacing;
	
	__block CGFloat itemWidth = 0.0f;
	
	if (self.itemSpacing) {
		itemWidth = (self.frameWidth - ([itemSpacing floatValue] * (items.count - 1))) / items.count;
	} else {
		itemWidth = self.frameWidth / items.count;
	}
	
	NSMutableArray *tabItems = [NSMutableArray new];
	
	__weak TMTabControl *wSelf = self;
	[items enumerateObjectsUsingBlock:^(NSString *tabItem, NSUInteger idx, BOOL *stop) {
		CGFloat xPosition = (itemWidth * idx) + (idx > 0 ? [itemSpacing floatValue] * idx : 0);
		UILabel *tabLabel = [[UILabel alloc] initWithFrame:CGRectMake(xPosition, 0.0f, itemWidth, self.frameHeight)];
		
		tabLabel.text                 = tabItem;
		tabLabel.textAlignment        = [textAttributes[@"textAlignment"] integerValue];
		tabLabel.textColor            = textAttributes[@"textColor"];
		tabLabel.font                 = textAttributes[@"font"];
		tabLabel.highlightedTextColor = textAttributes[@"highlightedTextColor"] ?: self.tintColor;
		if( wSelf.delegate && ![wSelf.delegate indexIsSelectable:idx]) {
			tabLabel.userInteractionEnabled = NO;
			tabLabel.alpha = 0.5f;
		}
		
		if (idx == 0) {
			tabLabel.highlighted = YES;
		}
		
		[self addSubview:tabLabel];
		
		[tabItems addObject:tabLabel];
	}];
	
	self.tabs = tabItems;
	
	CGFloat selectedTabViewY = 0.0f;
	
	if (tabLocation == TMTabLocationBottom) {
		selectedTabViewY = self.frameHeight - 3.0f;
	}
	
	self.selectedTabView = [[UIView alloc] initWithFrame:CGRectMake(0.0f, selectedTabViewY, itemWidth, 3.0f)];
	self.selectedTabView.backgroundColor = self.tintColor;
	
	[self addSubview:self.selectedTabView];
}

- (void)configureWithItems:(NSArray *)items textAttributes:(NSDictionary *)textAttributes colors:(NSDictionary *)colors {
	CGFloat itemWidth = self.frameWidth / items.count;
	NSMutableArray *tabItems = [NSMutableArray arrayWithCapacity:items.count];
	
	self.normalBackgroundColor      = colors[@"normalBackgroundColor"];
	self.highlightedBackgroundColor = colors[@"highlightedBackgroundColor"];

	__weak TMTabControl *wSelf = self;
	[items enumerateObjectsUsingBlock:^(NSString *tabItem, NSUInteger idx, BOOL *stop) {
		CGFloat xPosition = itemWidth * idx;
		UILabel *tabLabel = [[UILabel alloc] initWithFrame:CGRectMake(xPosition, 0.0f, itemWidth, self.frameHeight)];
		
		tabLabel.text                 = tabItem;
		tabLabel.textAlignment        = [textAttributes[@"textAlignment"] integerValue];
		tabLabel.textColor            = textAttributes[@"textColor"];
		tabLabel.font                 = textAttributes[@"font"];
		tabLabel.backgroundColor      = idx == 0 ? self.highlightedBackgroundColor : self.normalBackgroundColor;
		tabLabel.numberOfLines        = 0;
		tabLabel.highlightedTextColor = textAttributes[@"highlightedTextColor"] ?: self.tintColor;
		if( wSelf.delegate && ![wSelf.delegate indexIsSelectable:idx]) {
			tabLabel.userInteractionEnabled = NO;
			tabLabel.alpha = 0.5f;
		}
		
		[self addSubview:tabLabel];
		
		[tabItems addObject:tabLabel];
		
		if (idx == 0) {
			tabLabel.highlighted = YES;
		}
	}];
	
	self.tabs = tabItems;
}

- (void)selectTabAtIndex:(NSInteger)index {
	self.selectedIndex = index;
	
	if (self.delegate) {
		[self.delegate tabControlDidSelectItemAtIndex:index];
	}
	
	[self.tabs enumerateObjectsUsingBlock:^(UILabel *tabLabel, NSUInteger idx, BOOL *stop) {
		tabLabel.highlighted = index == idx;
		
		if (index == idx) {
			if (self.selectedTabView) {
				[UIView animateWithDuration:0.5 delay:0.0 usingSpringWithDamping:0.7 initialSpringVelocity:1.0 options:0 animations:^{
					self.selectedTabView.frameLeft = tabLabel.frameLeft;
				} completion:nil];
			} else {
				tabLabel.backgroundColor = self.highlightedBackgroundColor;
			}
		}
	}];
}


#pragma mark - TOUCH HANDLING -

- (void)endTrackingWithTouch:(UITouch *)touch withEvent:(UIEvent *)event {
	__block CGPoint touchPoint = [touch locationInView:self];
	__weak TMTabControl *wSelf = self;
	[self.tabs enumerateObjectsUsingBlock:^(UILabel *tabLabel, NSUInteger idx, BOOL *stop) {
		// If this index shouldn't be considered selectable, then skip it.
		if( wSelf.delegate && ![self.delegate indexIsSelectable:idx]) {
			return;
		}
		if (CGRectContainsPoint(tabLabel.frame, touchPoint)) {
			self.selectedIndex = idx;
			tabLabel.highlighted = YES;
			
			if (self.delegate) {
				[self.delegate tabControlDidSelectItemAtIndex:idx];
			}
			
			if (self.selectedTabView) {
				[UIView animateWithDuration:0.5 delay:0.0 usingSpringWithDamping:0.7 initialSpringVelocity:1.0 options:0 animations:^{
					self.selectedTabView.frameLeft = tabLabel.frameLeft;
				} completion:nil];
			} else {
				tabLabel.backgroundColor = self.highlightedBackgroundColor;
			}
		} else {
			if (!self.selectedTabView) {
				tabLabel.backgroundColor = self.normalBackgroundColor;
			}
			
			tabLabel.highlighted = NO;
		}
	}];
}

@end