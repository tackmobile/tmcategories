
#import "UIImage+TMHelpers.h"

@implementation UIImage (TMHelpers)

- (UIColor *)averageColor {
  CGImageRef rawImageRef = [self CGImage];
  
	CFDataRef data = CGDataProviderCopyData(CGImageGetDataProvider(rawImageRef));
  const UInt8 *rawPixelData = CFDataGetBytePtr(data);
  
  NSUInteger imageHeight = CGImageGetHeight(rawImageRef);
  NSUInteger imageWidth  = CGImageGetWidth(rawImageRef);
  NSUInteger bytesPerRow = CGImageGetBytesPerRow(rawImageRef);
	NSUInteger stride = CGImageGetBitsPerPixel(rawImageRef) / 8;
  
  unsigned int red   = 0;
  unsigned int green = 0;
  unsigned int blue  = 0;
  
	for (int row = 0; row < imageHeight; row++) {
		const UInt8 *rowPtr = rawPixelData + bytesPerRow * row;
		
    for (int column = 0; column < imageWidth; column++) {
      red    += rowPtr[0];
      green  += rowPtr[1];
      blue   += rowPtr[2];
      
      rowPtr += stride;
    }
  }
  
	CFRelease(data);
  
	CGFloat f = 1.0f / (255.0f * imageWidth * imageHeight);
  
  CGFloat hue;
  CGFloat sat;
  CGFloat bri;
  CGFloat alpha;
  
  UIColor *averageColor = [UIColor colorWithRed:f * red  green:f * green blue:f * blue alpha:1];
  [averageColor getHue:&hue saturation:&sat brightness:&bri alpha:&alpha];
	
  return [UIColor colorWithHue:hue saturation:sat brightness:bri alpha:alpha];
}

- (UIImage *)tintedImageWithColor:(UIColor *)tintColor {
  UIImage *imageToTint = self;
  UIGraphicsBeginImageContextWithOptions(imageToTint.size, NO, 0.0f);
  [tintColor setFill];
  CGRect bounds = CGRectMake(0, 0, imageToTint.size.width, imageToTint.size.height);
  UIRectFill(bounds);
  [imageToTint drawInRect:bounds blendMode:kCGBlendModeDestinationIn alpha:1.0f];
  
  UIImage *tintedImage = UIGraphicsGetImageFromCurrentImageContext();
  UIGraphicsEndImageContext();
  
  return tintedImage;
}

@end
