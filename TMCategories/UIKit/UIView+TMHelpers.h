
@interface UIView (TMHelpers)

// Bounds Positioning
@property (readonly, nonatomic) CGFloat boundsBottom;
@property (readonly, nonatomic) CGFloat boundsLeft;
@property (readonly, nonatomic) CGFloat boundsRight;
@property (readonly, nonatomic) CGFloat boundsTop;
@property (readonly, nonatomic) CGPoint boundsPosition;
@property (readonly, nonatomic) CGPoint boundsCenter;

// Bounds Size
@property (readonly, nonatomic) CGFloat boundsHeight;
@property (readonly, nonatomic) CGFloat boundsWidth;
@property (readonly, nonatomic) CGSize  boundsSize;

// Frame Positioning
@property (assign, nonatomic) CGFloat   frameBottom;
@property (assign, nonatomic) CGFloat   frameLeft;
@property (assign, nonatomic) CGFloat   frameRight;
@property (assign, nonatomic) CGFloat   frameTop;
@property (assign, nonatomic) CGPoint   framePosition;
@property (assign, nonatomic) CGFloat   frameX;
@property (assign, nonatomic) CGFloat   frameY;

// Frame Size
@property (assign, nonatomic) CGFloat   frameHeight;
@property (assign, nonatomic) CGFloat   frameWidth;
@property (assign, nonatomic) CGSize    frameSize;

#if !defined(TM_APP_EXTENSIONS)
+ (CGRect)getScreenBoundsForCurrentOrientation;
#endif

+ (CGRect)getScreenBoundsForOrientation:(UIInterfaceOrientation)orientation;

@end