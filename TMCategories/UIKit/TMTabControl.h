
typedef NS_ENUM(NSInteger, TMTabLocation) {
  TMTabLocationBottom,
  TMTabLocationTop
};

@protocol TMTabControlDelegate <NSObject>

- (void)tabControlDidSelectItemAtIndex:(NSInteger)index;
- (BOOL)indexIsSelectable:(NSInteger)index;

@end

@interface TMTabControl : UIControl

@property (weak, nonatomic) IBOutlet id <TMTabControlDelegate> delegate;

- (void)configureWithItems:(NSArray *)items itemSpacing:(NSNumber *)itemSpacing tabLocation:(TMTabLocation)tabLocation textAttributes:(NSDictionary *)textAttributes;
- (void)configureWithItems:(NSArray *)items textAttributes:(NSDictionary *)textAttributes colors:(NSDictionary *)colors;
- (void)selectTabAtIndex:(NSInteger)index;

@end