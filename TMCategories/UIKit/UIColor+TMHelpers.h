
@interface UIColor (TMHelpers)

/**
 Returns a UIColor object for the given hex string.
 
 @param hexString The hex string to convert to a color. Usually in the format #a1b2c3.
*/
+ (UIColor *)colorWithHexString:(NSString *)hexString;
/**
 Returns a UIColor object for the given hex string and alpha value.
 
 @param hexString The hex string to convert to a color. Usually in the format #a1b2c3.
 @param alpha The alpha value 0—1.0.
*/
+ (UIColor *)colorWithHexString:(NSString *)hexString alpha:(CGFloat)alpha;
/**
 Returns a UIColor object for the given HSBA values. H is NSInteger 0—360, while SBA is CGFloat 0—1.0.
 
 @param hue The hue value 0—360.
 @param saturation The saturation value 0—1.0.
 @param brightness The brightness value 0—1.0.
 @param alpha The alpha value 0—1.0.
*/
+ (UIColor *)colorWithH:(NSInteger)hue S:(CGFloat)saturation B:(CGFloat)brightness A:(CGFloat)alpha;
/**
 Returns a UIColor object for the given RGBA values. RGB are NSInteger 0—255, while alpha is CGFloat 0—1.0.
 
 @param red The red value 0—255.
 @param green The green value 0—255.
 @param blue The blue value 0—255.
 @param alpha The alpha value 0—1.0.
*/
+ (UIColor *)colorWithR:(NSInteger)red G:(NSInteger)green B:(NSInteger)blue A:(CGFloat)alpha;

@end