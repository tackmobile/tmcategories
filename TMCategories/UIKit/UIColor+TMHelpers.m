
#import "UIColor+TMHelpers.h"

@implementation UIColor (TMHelpers)


#pragma mark - CLASS METHODS -

+ (UIColor *)colorWithHexString:(NSString *)hexString {
  return [UIColor colorWithHexString:hexString alpha:1.0];
}

+ (UIColor *)colorWithHexString:(NSString *)hexString alpha:(CGFloat)alpha {
  unsigned int hexint = [self intFromHexString:hexString];
  
  UIColor *color = [UIColor colorWithRed:((CGFloat) ((hexint & 0xFF0000) >> 16)) / 255 green:((CGFloat) ((hexint & 0xFF00) >> 8)) / 255 blue:((CGFloat) (hexint & 0xFF)) / 255 alpha:alpha];
  
  return color;
}

+ (UIColor *)colorWithH:(NSInteger)hue S:(CGFloat)saturation B:(CGFloat)brightness A:(CGFloat)alpha {
  CGFloat hueValue = ABS(hue);
  
  hueValue = MIN(hueValue, 360) / 360.0;
  
  return [UIColor colorWithHue:hueValue saturation:saturation brightness:brightness alpha:alpha];
}

+ (UIColor *)colorWithR:(NSInteger)red G:(NSInteger)green B:(NSInteger)blue A:(CGFloat)alpha {
  CGFloat redValue = ABS(red);
  CGFloat greenValue = ABS(green);
  CGFloat blueValue = ABS(blue);
  
  redValue = MIN(redValue, 255) / 255.0;
  greenValue = MIN(greenValue, 255) / 255.0;
  blueValue = MIN(blueValue, 255) / 255.0;
  
  return [UIColor colorWithRed:redValue green:greenValue blue:blueValue alpha:alpha];
}

#pragma mark - INTERNAL METHODS -

+ (unsigned int)intFromHexString:(NSString *)hexStr {
  unsigned int hexInt = 0;
  
  NSScanner *scanner = [NSScanner scannerWithString:hexStr];
  [scanner setCharactersToBeSkipped:[NSCharacterSet characterSetWithCharactersInString:@"#"]];
  [scanner scanHexInt:&hexInt];
  
  return hexInt;
}

@end