
@interface NSDate (TMHelpers)

- (NSDate *)dayBoundary;
- (NSDate *)endOfWeekBoundary;
- (NSDate *)monthBoundary;
- (NSDate *)weekBoundary;
- (NSDate *)yearBoundary;

- (NSRange)monthRange;

@end