
#import "NSDate+TMHelpers.h"

@implementation NSDate (TMHelpers)


#pragma mark - INSTANCE METHODS -

- (NSDate *)dayBoundary {
  return [self boundaryForCalendarUnit:NSCalendarUnitDay];
}

- (NSDate *)endOfWeekBoundary {
  NSDate *weekBoundary = [self weekBoundary];
  NSDateComponents *dateComponents = [[NSDateComponents alloc] init];
  dateComponents.day = 6;
  
  return [[NSCalendar currentCalendar] dateByAddingComponents:dateComponents toDate:weekBoundary options:0];
}

- (NSDate *)monthBoundary {
  return [self boundaryForCalendarUnit:NSCalendarUnitMonth];
}

- (NSDate *)weekBoundary {
  return [self boundaryForCalendarUnit:NSCalendarUnitWeekOfYear];
}

- (NSDate *)yearBoundary {
  return [self boundaryForCalendarUnit:NSCalendarUnitYear];
}

- (NSRange)monthRange {
  return [[NSCalendar currentCalendar] rangeOfUnit:NSCalendarUnitDay inUnit:NSCalendarUnitMonth forDate:self];
}


#pragma mark - INTERNAL METHODS -

- (NSDate *)boundaryForCalendarUnit:(NSCalendarUnit)calendarUnit {
  NSDate *boundary;
  [[NSCalendar currentCalendar] rangeOfUnit:calendarUnit startDate:&boundary interval:NULL forDate:self];
  
  return boundary;
}

@end